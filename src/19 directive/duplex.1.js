//双工绑定
var duplexBinding = avalon.directive("duplex", {
  priority: 2000,
  update: function(value, elem, binding){
    if (binding.bound)
        return
    binding.changed = getBindingCallback(elem, "binding-duplex-changed", binding.vmodels) || noop
    var params = []
    var casting = oneObject("string,number,boolean,checked")
    if (elem.type === "radio" && binding.param === "") {
        binding.param = "checked"
    }
    if (elem.msbinding) {
        elem.msbinding["ms-duplex"] = binding.value
    }
    var hasCast
    binding.param.replace(/\w+/g, function (name) {
        if (/^(checkbox|radio)$/.test(elem.type) && /^(radio|checked)$/.test(name)) {
            if (name === "radio")
                log("ms-duplex-radio已经更名为ms-duplex-checked")
            name = "checked"
            binding.isChecked = true
        }
        if (name === "bool") {
            name = "boolean"
            log("ms-duplex-bool已经更名为ms-duplex-boolean")
        } else if (name === "text") {
            name = "string"
            log("ms-duplex-text已经更名为ms-duplex-string")
        }
        if (casting[name]) {
            hasCast = true
        }
        avalon.Array.ensure(params, name)
    })
    if (!hasCast) {
        params.push("string")
    }
    binding.param = params.join("-")
    binding.bound = function (type, callback) {
        if (elem.addEventListener) {
            elem.addEventListener(type, callback, false)
        } else {
            elem.attachEvent("on" + type, callback)
        }
        var old = binding.rollback
        binding.rollback = function () {
            elem.avalonSetter = null
            avalon.unbind(elem, type, callback)
            old && old()
        }
    }
    for (var i in avalon.vmodels) {
        var v = avalon.vmodels[i]
      //  v.$fire("avalon-ms-duplex-init", binding)
    }
    var cpipe = binding.pipe || (binding.pipe = pipe)
    cpipe(null, binding, "init")
    var tagName = elem.tagName
    duplexBinding[tagName] && duplexBinding[tagName](elem, binding.evaluator.apply(null, binding.args), binding)


  }
})


//不存在 bindingExecutors.duplex

function fixNull(val) {
    return val == null ? "" : val
}
avalon.duplexHooks = {
    checked: {
        get: function (val, data) {
            return !data.element.oldValue
        }
    },
    string: {
        get: function (val) { //同步到VM
            return val
        },
        set: fixNull
    },
    "boolean": {
        get: function (val) {
            return val === "true"
        },
        set: fixNull
    },
    number: {
        get: function (val, data) {
            var number = parseFloat(val)
            if (-val === -number) {
                return number
            }
            var arr = /strong|medium|weak/.exec(data.element.getAttribute("data-duplex-number")) || ["medium"]
            switch (arr[0]) {
                case "strong":
                    return 0
                case "medium":
                    return val === "" ? "" : 0
                case "weak":
                    return val
            }
        },
        set: fixNull
    }
}

function pipe(val, data, action, e) {
    data.param.replace(/\w+/g, function (name) {
        var hook = avalon.duplexHooks[name]
        if (hook && typeof hook[action] === "function") {
            val = hook[action](val, data)
        }
    })
    return val
}

var TimerID, ribbon = []

avalon.tick = function (fn) {
    if (ribbon.push(fn) === 1) {
        TimerID = setInterval(ticker, 60)
    }
}

function ticker() {
    for (var n = ribbon.length - 1; n >= 0; n--) {
        var el = ribbon[n]
        if (el() === false) {
            ribbon.splice(n, 1)
        }
    }
    if (!ribbon.length) {
        clearInterval(TimerID)
    }
}

var watchValueInTimer = noop
var rmsinput = /text|password|hidden/
new function () { // jshint ignore:line
    try { //#272 IE9-IE11, firefox
        var setters = {}
        var aproto = HTMLInputElement.prototype
        var bproto = HTMLTextAreaElement.prototype
        function newSetter(value) { // jshint ignore:line
            setters[this.tagName].call(this, value)
            if (rmsinput.test(this.type) && !this.msFocus && this.avalonSetter) {
                this.avalonSetter()
            }
        }
        var inputProto = HTMLInputElement.prototype
        Object.getOwnPropertyNames(inputProto) //故意引发IE6-8等浏览器报错
        setters["INPUT"] = Object.getOwnPropertyDescriptor(aproto, "value").set

        Object.defineProperty(aproto, "value", {
            set: newSetter
        })
        setters["TEXTAREA"] = Object.getOwnPropertyDescriptor(bproto, "value").set
        Object.defineProperty(bproto, "value", {
            set: newSetter
        })
    } catch (e) {
        //在chrome 43中 ms-duplex终于不需要使用定时器实现双向绑定了
        // http://updates.html5rocks.com/2015/04/DOM-attributes-now-on-the-prototype
        // https://docs.google.com/document/d/1jwA8mtClwxI-QJuHT7872Z0pxpZz8PBkf2bGAbsUtqs/edit?pli=1
        watchValueInTimer = avalon.tick
    }
} // jshint ignore:line
